var redis = require('redis'); //Import Redis
var client = redis.createClient();  // Create Redis Client Object.

client.on('connect', function () {
  console.log('Redis client is successfully connected');
});

client.on('error', function (err) {
  console.log('Something went wrong ', err);
});

//-------------- String -------------------

// client.set('Name', 'Anshul', redis.print);
// client.append('Name', 'Suneja', redis.print);
// client.get('Name', function(error, result) {
//   if (error) throw error;
//   console.log('My name is: ', result)
// });

//-------------- Set ----------------------

client.sadd('myFriends', 'Mayank' , redis.print);
client.sadd('myFriends', 'Arjun' , redis.print);
client.sadd('myFriends', 'Varun' , redis.print);
client.SMEMBERS('myFriends' , function(error, result) {
  if (error) throw error;
  console.log('My friends>>>>>', result);
});

//-------------- Sorted Set ---------------
// client.zadd('Ranks', 500, 'Arjun');
// client.zadd('Ranks', 200, 'Prateek');
// client.zadd('Ranks', 400, 'Rohit');
// client.zadd('Ranks', 100, 'Varun');
// client.zadd('Ranks', 300, 'Mayank');
// client.zrangebyscore('Ranks' , 100 ,300, function(error, result) {
//   if (error) throw error;
//   console.log('Ranks>>>>>', result);
// });


