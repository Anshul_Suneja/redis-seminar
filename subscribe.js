var redis = require('redis');
var subscriber = redis.createClient();

subscriber.on('message', function (channel, message) {         
 console.log(`Message : ${message} has arrived on channel : ${channel}`);
});

subscriber.subscribe('chats');